Source: fcitx5-fbterm
Section: utils
Priority: optional
Maintainer: Debian Input Method Team <debian-input-method@lists.debian.org>
Uploaders:
 Boyuan Yang <byang@debian.org>,
Build-Depends:
 cmake,
 debhelper-compat (= 13),
 libglib2.0-dev,
 libfcitx5utils-dev,
 libfcitx5gclient-dev,
Standards-Version: 4.6.1
Rules-Requires-Root: no
Homepage: https://github.com/fcitx/fcitx5-fbterm
Vcs-Git: https://salsa.debian.org/input-method-team/fcitx5-fbterm.git
Vcs-Browser: https://salsa.debian.org/input-method-team/fcitx5-fbterm

Package: fcitx5-frontend-fbterm
Architecture: linux-any
Depends:
 fbterm,
 fcitx5,
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 fcitx5-chinese-addons,
Description: FbTerm frontend for fcitx5
 Fcitx5 is the next generation of fcitx input method framework. It
 provides pleasant and modern input experience with intuitive graphical
 configuration tools. The framework is highly extensible with support
 for GTK+ and Qt toolkits, DBus interfaces, a large variety of desktop
 environments and a developer-friendly API.
 .
 Some of its new features include support of both Wayland and Xorg
 and the ability to mimic IBus Input Method Framework in order to
 provide better compatibility across different Linux distributions
 and desktop environments.
 .
 This package provides the FbTerm frontend, which is recommended for
 users who do not use X.
